package com.roshless.gitstats;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class GitCommits {
    private String name;
    private String email;

    private LinkedHashMap<String, Date> commits;

    public GitCommits(String name, String email, LinkedHashMap<String, Date> commits) {
        this.name = name;
        this.email = email;
        this.commits = commits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LinkedHashMap<String, Date> getCommits() {
        return commits;
    }

    public void setCommits(LinkedHashMap<String, Date> commits) {
        this.commits = commits;
    }
}
