package com.roshless.gitstats;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class GitOperations {
    private String URL;
    private GitRepoStats gitRepoStats;
    private List<GitUser> userList;
    private List<RevCommit> logs;

    public String getURL() {
        return URL;
    }

    public GitOperations(String URL) {
        this.URL = URL;
        userList = new ArrayList<>();
        logs = new ArrayList<>();
        try {
            cloneRepo(this.URL);
        } catch (IOException | GitAPIException e) { //handle higher?
            e.printStackTrace();
        }
    }

    private void cloneRepo(String URL) throws IOException, GitAPIException {
        Path tempDirWithPrefix = Files.createTempDirectory("gitstats");
        if (!URL.startsWith("https") || !URL.startsWith("http"))
            URL = "https://" + URL;


        Git git = Git.cloneRepository()
                .setNoCheckout(true)
                .setURI(URL)
                .setDirectory(tempDirWithPrefix.toFile())
                .call();

        gitRepoStats = new GitRepoStats(URL, 0, 0);
        Iterable<RevCommit> log = git.log().all().call();
        log.forEach(logs::add);
        logs.forEach(revCommit -> {
            PersonIdent author = revCommit.getAuthorIdent();
            GitUser gitUser = new GitUser(author.getName(), author.getEmailAddress(), 1);
            boolean found = false;
            for (int i = 0; i < userList.size(); i++) {
                if (userList.get(i).equals(gitUser)) {
                    GitUser newUser = userList.get(i);
                    newUser.setCommitCount(newUser.getCommitCount() + 1);
                    newUser.setCommitPercentage(logs.size());
                    userList.set(i, newUser);
                    found = true;
                }
            }
            if (!found) {
                gitUser.setCommitPercentage(logs.size());
                userList.add(gitUser);
            }
        });
        gitRepoStats.setAllCommits(logs.size());
        gitRepoStats.setContributors(userList.size());
    }

    public List<GitUser> getSortedOutput() {
        userList.sort(Comparator.comparing(GitUser::getCommitCount).reversed());
        return userList;
    }

    public Optional<GitUser> getUser(String email) {
        return userList.stream().filter(gitUser -> gitUser.getEmail().equals(email)).findFirst();
    }

    public GitRepoStats getGitRepoStats() {
        return gitRepoStats;
    }

    public GitCommits getCommitsFromEmail(String email) {
        LinkedHashMap<String, Date> commitMessages = new LinkedHashMap<>();
        String name = "";
        for (RevCommit commit : logs) {
            if (commit.getAuthorIdent().getEmailAddress().equals(email)) {
                commitMessages.put(commit.getFullMessage().trim(), commit.getAuthorIdent().getWhen());
                name = commit.getAuthorIdent().getName();
            }
        }
        if (commitMessages.size() == 0)
            return null;
        return new GitCommits(name, email, commitMessages);
    }
}