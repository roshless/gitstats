package com.roshless.gitstats;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.swagger.annotations.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import java.text.SimpleDateFormat;
import java.util.*;

@Api(value = "Simple application for checking  primary stats on remote repository.", description = "Operations pertaining to check primary statistics of repository")
@RestController
public class API {
    private GitOperations gitOperations;
    private ObjectWriter ow;

    public API() {
        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        mapper.setDateFormat(df);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }

    @ApiOperation(value = "Display number of contributors, all commits and name of repository", response = Json.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved JsON with stats"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> summary(@ApiParam(value = "Url of remote repository you want to check", example = "https://gitlab.com/Roshless/habit-rabbit-tracker") @RequestParam(value = "url", required = false) String repoURL) throws JsonProcessingException {
        if (gitOperations == null && repoURL == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ow.writeValueAsString(new Error("Use /user first or provide repository url")));
        else if (gitOperations == null || !gitOperations.getURL().equals(repoURL))
            gitOperations = new GitOperations(repoURL);

        return ResponseEntity.status(HttpStatus.OK).body(ow.writeValueAsString(gitOperations.getGitRepoStats()));
    }

    @ApiOperation(value = "Display list of contributors with details like: username, email, number of commits, percentage of involvement in the project", response = Json.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of contributors"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> commit(@ApiParam(value = "Url of remote repository you want to check", example = "https://gitlab.com/Roshless/habit-rabbit-tracker") @RequestParam(value = "url") String repoURL) throws JsonProcessingException {
        if (gitOperations == null || !gitOperations.getURL().equals(repoURL))
            gitOperations = new GitOperations(repoURL);

        return ResponseEntity.status(HttpStatus.OK).body(ow.writeValueAsString(gitOperations.getSortedOutput()));
    }

    @ApiOperation(value = "Display specified contributor with details like: username, email, number of commits, percentage of involvement in the project", response = Json.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved specified contributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> commitId(@ApiParam(value = "ID of contributor in project, starting from 0", example = "0") @PathVariable(value = "id") final Integer id,
                                           @ApiParam(value = "Url of remote repository you want to check", example = "https://gitlab.com/Roshless/habit-rabbit-tracker") @RequestParam(value = "url") String repoURL) throws JsonProcessingException {
        if (gitOperations == null || !gitOperations.getURL().equals(repoURL))
            gitOperations = new GitOperations(repoURL);

        try {
            return ResponseEntity.status(HttpStatus.OK).body(ow.writeValueAsString(gitOperations.getSortedOutput().get(id)));
        } catch (IndexOutOfBoundsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ow.writeValueAsString(new Error("User not found")));
        }
    }

    @ApiOperation(value = "Display list of commits of specified contributor", response = Json.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list commits"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "/commit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> details(@ApiParam(value = "Email address of contributor", example = "kamilrsw@gmail.com") @RequestParam(value = "email") String email,
                                          @ApiParam(value = "Url of remote repository you want to check", example = "https://gitlab.com/Roshless/habit-rabbit-tracker") @RequestParam(value = "url", required = false) String repoURL) throws JsonProcessingException {
        if (repoURL != null) {
            if (gitOperations == null || !gitOperations.getURL().equals(repoURL))
                gitOperations = new GitOperations(repoURL);
        } else if (gitOperations == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ow.writeValueAsString(new Error("Use /user first or provide url parameter")));

        GitCommits commits = gitOperations.getCommitsFromEmail(email);
        return ResponseEntity.status(HttpStatus.OK).body(ow.writeValueAsString(commits));
    }

    @ApiOperation(value = "Display specified commit of desired contributor", response = Json.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved commit"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "/commit/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> detailsId(@ApiParam(value = "ID of commit specified contributor, starting from 0", example = "1") @PathVariable(value = "id") final Integer id,
                                            @ApiParam(value = "Email address of contributor", example = "patryk_m77@wp.pl") @RequestParam(value = "email") String email,
                                            @ApiParam(value = "Url of remote repository you want to check", example = "https://gitlab.com/Roshless/habit-rabbit-tracker") @RequestParam(value = "url", required = false) String repoURL) throws JsonProcessingException {
        if (repoURL != null) {
            if (gitOperations == null || !gitOperations.getURL().equals(repoURL))
                gitOperations = new GitOperations(repoURL);
        } else if (gitOperations == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ow.writeValueAsString(new Error("Use /user first or provide url parameter")));

        GitCommits commits = gitOperations.getCommitsFromEmail(email);
        Object key, value;
        try {
            key = commits.getCommits().keySet().toArray()[id];
            value = commits.getCommits().values().toArray()[id];
        } catch (ArrayIndexOutOfBoundsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ow.writeValueAsString(new Error("User not found")));
        }
        Map<Object, Object> out = new HashMap<Object, Object>() {{
            put(key, value);
        }};
        return ResponseEntity.status(HttpStatus.OK).body(ow.writeValueAsString(out));
    }
}
