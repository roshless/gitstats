package com.roshless.gitstats;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionConfiguration extends ResponseEntityExceptionHandler {
    @ExceptionHandler(JsonProcessingException.class)
    public ResponseEntity<Error> handleConverterErrors(JsonProcessingException exception) {
        return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body(new Error("conversion to json was corrupted"));
    }
}
