package com.roshless.gitstats;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class GitUser implements Comparable<GitUser> {
    private String name;
    private String email;
    @JsonProperty(value = "commit_count")
    private int commitCount;
    @JsonProperty(value = "commit_percentage")
    private float commitPercentage;

    public int getCommitCount() {
        return commitCount;
    }

    public void setCommitCount(int commitCount) {
        this.commitCount = commitCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getCommitPercentage() {
        return commitPercentage;
    }

    public void setCommitPercentage(int allCommits) {
        this.commitPercentage = (float) commitCount / allCommits;
    }

    public GitUser(String name, String email, int commitCount) {
        this.name = name;
        this.email = email;
        this.commitCount = commitCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GitUser gitUser = (GitUser) o;
        return Objects.equals(name, gitUser.name) &&
                Objects.equals(email, gitUser.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }

    @Override
    public String toString() {
        return name + " <" + email + "> ";
    }

    @Override
    public int compareTo(GitUser gitUser) {
        return 0;
    }
}
